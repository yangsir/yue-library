package ai.yue.library.data.jdbc.constant;

/**
 * 更新类型
 * 
 * @author	孙金川
 * @since	2018年8月29日
 */
public enum DBUpdateEnum {

	正常, 
	递增, 
	递减, 
	递减_无符号;

}
