package ai.yue.library.template.constant;

/**
 * @author  孙金川
 * @version 创建时间：2018年7月26日
 */
public enum UserStatusEnum {
	
	正常, 冻结, 删除;
	
}
