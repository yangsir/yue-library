package ai.yue.library.base.constant;

/**
 * 排序方式
 * 
 * @author	孙金川
 * @since	2018年8月29日
 */
public enum SortEnum {

	降序, 
	升序;

}
